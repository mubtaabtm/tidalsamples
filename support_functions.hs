wordsWhen     :: (Char -> Bool) -> String -> [String]
wordsWhen p s =  case dropWhile p s of
                      "" -> []
                      s' -> w : wordsWhen p s''
                            where (w, s'') = break p s'

spaceSplit x = wordsWhen (==' ') x

join sep xs = foldr (\a b-> a ++ if b=="" then b else sep ++ b) "" xs
spJoin xs = join " " xs
spJoini xs = spJoin (map show xs)

multI :: (String, Int) -> String
multI (s, n) = (r) where 
  l = spaceSplit s :: [String]
  li = map read l :: [Int] 
  lni = map (*n) li :: [Int]
  ls = map show lni :: [String] 
  r = spJoin ls :: String

-- Constants (Ascending)
-- pi = pi -- Pi is Haskell builtin
em = 0.5772156649 :: Float -- Euler–Mascheroni constant
zeta3 = 1.2020569:: Float -- Apéry's constant
pythag = 1.41421 :: Float -- sqrt(2) Pythagoras' constant
gold = 1.6180339887498948482 :: Float -- Golden Ratio
freigen_a = 2.5029 :: Float -- The Feigenbaum constant α
khick = 2.6854520010 :: Float -- Khinchin's constant K
e = 2.7182818284  :: Float -- Euler's number
levy = 3.27582 :: Float -- Lévy's constant
fibr = 3.35988 :: Float -- Reciprocal Fibonacci constant 
freigen_d = 4.6692 :: Float -- The Feigenbaum constant δ
tau = 6.283185307179586 :: Float -- Pi*2
belph = 1000000000000066600000000000001 :: Float -- Belphegor's prime number
u = 1 :: Float 
-- List constants
fib = [0,1,1,2,3,5,8,13,21,34,55,89,144,233,377,610,987,1597,2584,4181,6765] :: [Float]
softexp = [0.0, 0.001, 0.012, 0.037,
          0.082, 0.151, 0.249, 0.379,
          0.545, 0.75, 1.0] :: [Float]

main = do
  let x = 300*1.0e-3
  print x